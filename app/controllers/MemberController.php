<?php

class MemberController extends BaseController {

    public function getList() {
        //get list user 
        $infos =DB::table('member_info')->get();
        $total =DB::table('member_info')->count();
        
        //assing to view 
        return View::make('member/list',array('infos' => $infos,"total" =>$total));
    }
    
    public function registMember()
    {
        
        if ($_POST)
        {
            $rules = array(
                'name' => array('required'),
                'email'    => array('required'),
            );

            $validation = Validator::make(Input::all(), $rules);

            if ($validation->fails())
            {
                 // Validation has failed.
                return Redirect::to('member/regist')->withErrors($validation);
            }

            //get data 
            DB::table('member_info')->insert(
                array('name' =>  $_POST["name"], 
                      'email' => $_POST["email"],
                      'address' => $_POST["address"],
                    )
            );
            return Redirect::guest('member/list');
            
        }
        else 
        {
            return View::make('member/regist');
        }
    }

}