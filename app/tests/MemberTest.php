<?php

class MemberTest extends TestCase {
    public function testSomethingIsTrue()
    {
        $this->assertTrue(true);
    }
    public function testEmpty()
    {
        $stack = array(1,2);
        $this->assertEmpty($stack);
 
        return $stack;
    }
    public function testOne()
    {
        $this->assertTrue(FALSE);
    }
    public function testTotalRowMore2FromControllerMemberList()
    {
        $response = $this->action('GET', 'MemberController@getList');
        $view=$response->original;
        
        $this->assertEquals(2, count($view["infos"]));
    }
}